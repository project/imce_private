<?php

namespace Drupal\imce_private\Plugin\CKEditor5Plugin;

use Drupal\ckeditor5\Plugin\CKEditor5PluginDefault;

/**
 * Defines Imce public link plugin for CKEditor5.
 *
 * @CKEditor5Plugin(
 *   id = "imce_private_public_link",
 *   ckeditor5 = @CKEditor5AspectsOfCKEditor5Plugin(
 *     plugins = { "imceprivate.ImcePublicLink" },
 *   ),
 *   drupal = @DrupalAspectsOfCKEditor5Plugin(
 *     label = @Translation("Imce Public Link"),
 *     library = "imce_private/drupal.imce_private.ckeditor5",
 *     admin_library = "imce_private/drupal.imce_private.admin",
 *     elements = {
 *       "<img>",
 *       "<img src alt height width data-entity-type data-entity-uuid>",
 *     },
 *     toolbar_items = {
 *       "imce_private_public_link" = {
 *         "label" = "Insert public link using Imce File Manager",
 *       },
 *     },
 *   ),
 * )
 */
class ImceCKEditor5PublicLink extends CKEditor5PluginDefault {

}

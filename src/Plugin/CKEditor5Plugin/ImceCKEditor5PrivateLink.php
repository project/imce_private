<?php

namespace Drupal\imce_private\Plugin\CKEditor5Plugin;

use Drupal\ckeditor5\Plugin\CKEditor5PluginDefault;

/**
 * Defines Imce private link plugin for CKEditor5.
 *
 * @CKEditor5Plugin(
 *   id = "imce_private_link",
 *   ckeditor5 = @CKEditor5AspectsOfCKEditor5Plugin(
 *     plugins = { "imceprivate.ImcePrivateLink" },
 *   ),
 *   drupal = @DrupalAspectsOfCKEditor5Plugin(
 *     label = @Translation("Imce Private Link"),
 *     library = "imce_private/drupal.imce_private.ckeditor5",
 *     admin_library = "imce_private/drupal.imce_private.admin",
 *     elements = {
 *       "<img>",
 *       "<img src alt height width data-entity-type data-entity-uuid>",
 *     },
 *     toolbar_items = {
 *       "imce_private_link" = {
 *         "label" = "Insert private link using Imce File Manager",
 *       },
 *     },
 *   ),
 * )
 */
class ImceCKEditor5PrivateLink extends CKEditor5PluginDefault {

}

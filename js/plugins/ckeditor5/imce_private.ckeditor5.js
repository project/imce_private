/* eslint max-classes-per-file: ["error", 4] */

((Drupal, CKEditor5) => {
  /**
   * @file
   * Defines Imce Private plugins for CKEditor5.
   */

  /**
   * Extend window.imceInput.
   */
  const imcePrivateInput = window.imceInput || {};

  /**
   * Defines imce_private.ImcePrivateImage plugin.
   *
   * Provides a button that inserts multiple images from Imce.
   */
  class ImcePrivateImage extends CKEditor5.core.Plugin {
    init() {
      const label = Drupal.t('Insert private images using Imce File Manager');
      imcePrivateInput.ckeditor5PluginInit(this.editor, 'private_image', label);
    }
  }

  /**
   * Defines imce_private.ImcePublicImage plugin.
   *
   * Provides a button that inserts multiple images from Imce.
   */
  class ImcePublicImage extends CKEditor5.core.Plugin {
    init() {
      const label = Drupal.t('Insert public images using Imce File Manager');
      imcePrivateInput.ckeditor5PluginInit(
        this.editor,
        'private_public_image',
        label,
      );
    }
  }

  /**
   * Defines imce_private.ImcePrivateLink plugin.
   *
   * Provides a button that inserts multiple links from Imce.
   */
  class ImcePrivateLink extends CKEditor5.core.Plugin {
    init() {
      const label = Drupal.t('Insert private link using Imce File Manager');
      imcePrivateInput.ckeditor5PluginInit(this.editor, 'private_link', label);
    }
  }

  /**
   * Defines imce_private.ImcePublicLink plugin.
   *
   * Provides a button that inserts multiple links from Imce.
   */
  class ImcePublicLink extends CKEditor5.core.Plugin {
    init() {
      const label = Drupal.t('Insert public link using Imce File Manager');
      imcePrivateInput.ckeditor5PluginInit(
        this.editor,
        'private_public_link',
        label,
      );
    }
  }

  /**
   * Add imce namespace.
   */
  CKEditor5.imceprivate = CKEditor5.imceprivate || {
    ImcePrivateImage,
    ImcePublicImage,
    ImcePrivateLink,
    ImcePublicLink,
  };

  /**
   * Init ckeditor5 image/link plugin.
   *
   * @param {string} editor - Editor.
   * @param {string} type - Type.
   * @param {string} label Label.
   */
  imcePrivateInput.ckeditor5PluginInit = (editor, type, label) => {
    const scheme = type.split('_').reverse()[1];
    const type2 = type.split('_').reverse()[0];
    editor.ui.componentFactory.add(`imce_${type}`, () => {
      const button = new CKEditor5.ui.ButtonView();
      const typeclass = type.replaceAll('_', '-');
      button.set({
        label,
        class: `ck-imce-button ck-imce-private-button ck-imce-${typeclass}-button`,
        tooltip: true,
      });
      button.on('execute', () => {
        const id = editor.sourceElement.getAttribute('data-ckeditor5-id');
        return imcePrivateInput.openImcePrivate(
          'imceInput.sendtoCKEditor5',
          type2,
          `ckid=${id}`,
          scheme,
        );
      });
      return button;
    });
  };

  imcePrivateInput.openImcePrivate = (sendto, type, params, scheme) => {
    const params2 = params ? `&${params}` : '';
    const url = imcePrivateInput.urlPrivate(
      `sendto=${sendto}&type=${type}${params2}`,
      scheme,
    );
    return window.imceInput.openWindow(url);
  };

  imcePrivateInput.urlPrivate = (params, scheme) => {
    let url = Drupal.url('imce');
    if (scheme) {
      url += `/${scheme}`;
    }
    if (params) {
      url += (url.indexOf('?') === -1 ? '?' : '&') + params;
    }
    return url;
  };
})(Drupal, CKEditor5);

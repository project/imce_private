## CONTENTS OF THIS FILE

  - Introduction
  - Requirements
  - Installation
  - Configuration
  - Troubleshooting
  - FAQ
  - Maintainers

## INTRODUCTION

  - The IMCE Private module adds IMCE buttons for private and public files
    to CKEditor, so you can access the non-default filesystem.

  - The module does not set any permissions or block access.

  - For a full description of the module visit:
    <https://www.drupal.org/project/imce_private>

  - To submit bug reports and feature suggestions, or to track changes visit:
    <https://www.drupal.org/project/issues/imce_private>

## REQUIREMENTS

  - [IMCE module](https://www.drupal.org/project/imce)

## RECOMMENDED MODULES

  - Private files download permission
    <https://www.drupal.org/project/private_files_download_permission>:
    sets access permissions for private filesystem

## INSTALLATION

Install the IMCE Private module as you would normally install a
contributed Drupal module. Visit <https://www.drupal.org/node/1897420> for
further information.

## CONFIGURATION

    1. Navigate to Administration > Configuration > Text formats and editors >
       Configure textformat which uses CKEditor.
    2. Drag one of the IMCE Private buttons to the active toolbar.
    3. Save configuration.

## TROUBLESHOOTING

## FAQ

## MAINTAINERS

  - sleitner - <https://www.drupal.org/u/sleitner>
